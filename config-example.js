// Устанавливаем токен, который выдавал нам бот.
let token = 'your_token';
let agentOptions = {
    socksHost: '00.00.000.000',
    socksPort: 1180,
    // If authorization is needed:
    socksUsername: 'proxy_user',
    socksPassword: 'proxy_password'
};

module.exports = { token: token, agentOptions: agentOptions };

