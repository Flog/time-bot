const config = require('./config');
const TelegramBot = require('node-telegram-bot-api');
const Agent = require('socks5-https-client/lib/Agent');

// Включить опрос сервера
const bot = new TelegramBot(config.token, {
    polling: true,
    request: {
        agentClass: Agent,
        agentOptions: config.agentOptions
    }
});



// tasks looks like: {id: 123, chatId: 456, startedAt: 1111111, status: 'started'}
let tasks = [];
const STATUS_IN_PROGRESS = 'in_progress';
const STATUS_DONE = 'done';
let getTime = function (task) {
    if (task.status === STATUS_DONE) {
        return task.endedAt - task.startedAt;
    }

    if (task.status === STATUS_IN_PROGRESS) {
        return Date.now() - task.startedAt;
    }
};

let timeConvert = function (duration) {
    let seconds = Math.floor((duration / 1000) % 60),
        minutes = Math.floor((duration / (1000 * 60)) % 60),
        hours = Math.floor((duration / (1000 * 60 * 60)) % 24);

    hours = (hours < 10) ? "0" + hours : hours;
    minutes = (minutes < 10) ? "0" + minutes : minutes;
    seconds = (seconds < 10) ? "0" + seconds : seconds;

    return hours + ":" + minutes + ":" + seconds;
};

bot.onText(/\/debug/, (msg) => {
    console.log("--------------------------------------------");
    console.log(tasks);
    console.log("--------------------------------------------");
});

bot.onText(/\/start/, (msg) => {

    for (let task of tasks) {
        if (task.chatId === msg.chat.id && task.status === STATUS_IN_PROGRESS) {
            if (task.id === null) {
                bot.sendMessage(msg.chat.id, "Таймер уже запущен. Нужно указать тэг задачи");
            } else {
                bot.sendMessage(msg.chat.id, "Есть задача в прогрессе: " + task.id);
            }
            return;
        }
    }
    bot.sendMessage(msg.chat.id, `Время пошло.`).then(function () {
        bot.sendMessage(
            msg.chat.id,
            "Отправь мне короткий тэг или id того что ты делаешь. По нему будет считаться время в конце дня. Например: тикет 233"
        ).then(function () {
            tasks.push({
                chatId: msg.chat.id,
                id: null,
                startedAt: Date.now(),
                status: STATUS_IN_PROGRESS,
                endedAt: null
            });
        });
    });


});
bot.on('message', (msg) => {
    let taskName = msg.text.toString().toLowerCase();
    if (taskName.indexOf('/') === 0) {
        return;
    }
    if (taskName !== '' && taskName.length > 2) {
        for (let task of tasks) {
            if (task.chatId === msg.chat.id
                && task.id === null
            ) {
                task.id = taskName;
                bot.sendMessage(msg.chat.id, `Тикет ${taskName} записан.`);
                return;
            }
        }
        bot.sendMessage(msg.chat.id, "К чему бы это? Введите команду которая начинается с /");
    } else {
        bot.sendMessage(msg.chat.id, "Что-то пошло не так. Тэг должен быть больше 2х символов. Отправь тэг еще разок.");
    }
});
bot.onText(/\/done/, (msg) => {
    for (let task of tasks) {
        if (task.chatId === msg.chat.id
            && task.status === STATUS_IN_PROGRESS
        ) {
            task.endedAt = Date.now();
            task.status = STATUS_DONE;
            bot.sendMessage(msg.chat.id, "Готово!");
            return;
        }
    }

    bot.sendMessage(msg.chat.id, `Ничего нет. Сначала начни задачу!`);

});

bot.onText(/\/list/, (msg) => {
    let inProgress = 0;
    let done = 0;
    let totalTime = 0;
    let promises = [];

    for (let task of tasks) {
        if (task.chatId === msg.chat.id) {
            let time = getTime(task);
            if (task.status === STATUS_DONE) {
                let promise = bot.sendMessage(msg.chat.id, `Задача завершена ${task.id}, заняла: ${timeConvert(time)}`);
                promises.push(promise);
                done++;
                totalTime += time;
            }

            if (task.status === STATUS_IN_PROGRESS) {
                let promise = bot.sendMessage(msg.chat.id, `Задача: ${task.id} в процессе ${timeConvert(time)}`);
                promises.push(promise);
                inProgress++;
                totalTime += time;
            }
        }
    }
    let textMsg = null;
    if (done === 0 && inProgress === 0) {
        textMsg = `Ничего нет. Сначала начните задачу!`;
    } else if (done !== 0 && inProgress === 0) {
        textMsg = `Ты сегодня умничка! Все задачи закрыты.`;
    }


    Promise.all(promises).then(values => {
        if (textMsg !== null) {
            bot.sendMessage(msg.chat.id, textMsg);
        }
    });

});
bot.onText(/\/total/, (msg) => {
    let totalTime = 0;

    for (let task of tasks) {
        if (task.chatId === msg.chat.id) {
            if (task.status === STATUS_DONE) {
                totalTime += task.endedAt - task.startedAt;
            }

            if (task.status === STATUS_IN_PROGRESS) {
                totalTime += Date.now() - task.startedAt;
            }
        }
    }

    totalTime = timeConvert(totalTime);
    bot.sendMessage(msg.chat.id, `Общее время работы ${totalTime}`);
});

bot.onText(/\/stat/, (msg) => {});